package com.hit.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class RandomHashSet<K> implements Iterable<K> {

	private final HashSet<K> hashSet;
	private final List<K> list;
	public RandomHashSet(int capacity) {
		hashSet = new HashSet<K>(capacity);
		list = new ArrayList<K>(capacity);
	}
	
	public void add(K k)
	{
		hashSet.add(k);
		list.add(k);
	}
	
	public boolean contains(K k)
	{
		return hashSet.contains(k);
	}
	
	public boolean isEmpty()
	{
		return list.isEmpty();
	}
	
	public K removeRandom()
	{
		int randomIndex = ThreadLocalRandom.current().nextInt(0, list.size());
		K toRemove = list.get(randomIndex);
		list.remove(randomIndex);
		hashSet.remove(toRemove);
		return toRemove;
	}
	
	public void remove(K toRemove)
	{
		if(!hashSet.contains(toRemove))
			return;
		list.remove(toRemove);
		hashSet.remove(toRemove);
	}
	
	public void clear()
	{
		list.clear();
		hashSet.clear();
	}

	@Override
	public Iterator<K> iterator() {
		return list.iterator();
	}
	
	
}
