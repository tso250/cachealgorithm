package com.hit.utils;
import java.util.TimerTask;

import com.hit.algorithm.NRUAlgoCacheImpl;

public class NRUTimerTask<K,V> extends TimerTask {

	private NRUAlgoCacheImpl<K, V> listener;
	public NRUTimerTask(NRUAlgoCacheImpl<K, V> nru) {
		listener = nru;
		
	}
	@Override
	public synchronized void run() {
		listener.clearRefernceBit();
	}
	

}




