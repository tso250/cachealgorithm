package com.hit.algorithm;
import java.util.Timer;
import java.util.TimerTask;

import com.hit.utils.NRUTimerTask;
import com.hit.utils.RandomHashSet;

public class NRUAlgoCacheImpl<K,V> extends AbstractAlgoCache<K, V> {

	private RandomHashSet<K> notRefNotMod;
	private RandomHashSet<K> notRefMod;
	private RandomHashSet<K> refNotMod;
	private RandomHashSet<K> refMod;
	
	private Timer timer;
	private TimerTask timerTask;
	
	public NRUAlgoCacheImpl(int capacity) {
		super(capacity);
		notRefNotMod = new RandomHashSet<K>(capacity);
		notRefMod = new RandomHashSet<K>(capacity);
		refNotMod = new RandomHashSet<K>(capacity);
		refMod = new RandomHashSet<K>(capacity);
		timer = new Timer();
		timerTask = new NRUTimerTask<K, V>(this);
		timer.scheduleAtFixedRate(timerTask, 0, 1000);
	}
	
	public synchronized void clearRefernceBit()
	{
		for (K it : refMod) {
			notRefMod.add(it);
		}
		refMod.clear();
		
		for (K it : refNotMod) {
			notRefNotMod.add(it);
		}
		refNotMod.clear();
	}
	
	public synchronized void clearAllFlags(K key)
	{
		notRefNotMod.remove(key);
		refNotMod.remove(key);
		notRefMod.remove(key);
		notRefNotMod.remove(key);
	}

	@Override
	public synchronized V getElement(K key) {
		if(isEmpty())
			return null;
		if(!getCache().containsKey(key))
			return null;
		V result = getCache().get(key);
		
		
		boolean isMod = refMod.contains(key) || notRefMod.contains(key);
		clearAllFlags(key);

		if(isMod){
			refMod.add(key);
		}
		else {
			refNotMod.add(key);
		}
			
		return result;
	}

	@Override
	public synchronized V putElement(K key, V value) {
		V valueToRemove = null;
		
		if(!isEmpty() && getCache().containsKey(key)){
			V exists = getCache().get(key);
			if(!exists.equals(value)){
			
				boolean isRef = refMod.contains(key) || refNotMod.contains(key);
				clearAllFlags(key);
				
				if(isRef){
					refMod.add(key);
				}
				else {
					notRefMod.add(key);
				}
			}
		}
		else{
			if(isFull()){
				K toRemove = null;
				
				if(!notRefNotMod.isEmpty()){
					toRemove = notRefNotMod.removeRandom();
				}
				else if(!notRefMod.isEmpty()){
					toRemove = notRefMod.removeRandom();
				}
				else if(!refNotMod.isEmpty()){
					toRemove = refNotMod.removeRandom();
				}
				else if(!refMod.isEmpty()){
					toRemove = refMod.removeRandom();
				}
				clearAllFlags(toRemove);
				valueToRemove = getCache().remove(toRemove);
				increaseAvailableCapacity();
			}
			
			getCache().put(key, value);
			clearAllFlags(key);
			notRefNotMod.add(key); // maybe referenced not modified?
			decreaseAvailableCapacity();
		}
		
		return valueToRemove;
	}

	@Override
	public synchronized void removeElement(K key) {
		if(isEmpty())
			return;
		
		if(!getCache().containsKey(key))
			return;
		
		getCache().remove(key);
		notRefNotMod.remove(key);
		notRefMod.remove(key);
		refNotMod.remove(key);
		refMod.remove(key);
		
		increaseAvailableCapacity();
		
	}

}
