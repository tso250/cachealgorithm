package com.hit.algorithm;

import java.util.LinkedList;
import java.util.Queue;

public class LRUAlgoCacheImpl<K, V> extends AbstractAlgoCache<K, V> {

	
	private Queue<K> lruQueue;
	
	public LRUAlgoCacheImpl(int capacity) {
		super(capacity);
		lruQueue = new LinkedList<K>();
	}

	@Override
	public V getElement(K key) {
		
		if(!getCache().containsKey(key))
			return null;
		
		V result = getCache().get(key);
		
		lruQueue.remove(key);
		lruQueue.add(key);
		
		return result;
	}

	@Override
	public V putElement(K key, V value) {
		
		V result = null;
		if(getElement(key) != null){
			getCache().put(key, value);
			return null;
		}
		
		if(isFull())
		{
			K lru = lruQueue.poll();
			result = getCache().remove(lru);
			increaseAvailableCapacity();
		}
		
		lruQueue.add(key);
		getCache().put(key, value);
		decreaseAvailableCapacity();
		
		return result;
	}

	@Override
	public void removeElement(K key) {
		if(isEmpty())
			return;
		
		if(!getCache().containsKey(key))
			return;
		
		getCache().remove(key);
		lruQueue.remove(key);
		
		increaseAvailableCapacity();
		
	}

}
