package com.hit.algorithm;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractAlgoCache<K, V> implements IAlgoCache<K, V> {
	private Map<K, V> cached;
	private final int capacity; 
	private int actualCapacity;
	
	AbstractAlgoCache(int capacity)
	{
		this.capacity = capacity;
		this.actualCapacity = this.capacity;
		this.cached = new HashMap<K, V>(capacity);
	}
	
	protected Map<K, V> getCache()
	{
		return cached;
	}
	
	public boolean isFull()
	{
		return this.actualCapacity == 0;
	}
	
	public boolean isEmpty()
	{
		return this.actualCapacity == capacity;
	}
	
	protected boolean decreaseAvailableCapacity()
	{
		if(isFull())
			return false;
		this.actualCapacity--;
		return true;
	}
	
	protected boolean increaseAvailableCapacity()
	{
		if(isEmpty())
			return false;
		this.actualCapacity++;
		return true;
	}
	
}
