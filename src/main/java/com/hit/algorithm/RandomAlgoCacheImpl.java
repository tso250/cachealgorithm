package com.hit.algorithm;
import com.hit.utils.RandomHashSet;

public class RandomAlgoCacheImpl<K, V> extends AbstractAlgoCache<K, V> {

	private RandomHashSet<K> randomCache;
	
	public RandomAlgoCacheImpl(int capacity) {
		super(capacity);
		randomCache = new RandomHashSet<K>(capacity);
	}

	@Override
	public V getElement(K key) {
		if(isEmpty())
			return null;
		if(!getCache().containsKey(key))
			return null;
		
		V result = getCache().get(key);
			
		return result;
	}

	@Override
	public V putElement(K key, V value) {
		V valueToRemove = null;
		
		if(isFull()){
			K toRemove = null;
			
			if(!randomCache.isEmpty()){
				toRemove = randomCache.removeRandom();
			}

			valueToRemove = getCache().remove(toRemove);
			increaseAvailableCapacity();
		}
		
		getCache().put(key, value);
		randomCache.add(key);
		decreaseAvailableCapacity();

		return valueToRemove;
	}

	@Override
	public void removeElement(K key) {
		if(isEmpty())
			return;
		
		if(!getCache().containsKey(key))
			return;
		
		getCache().remove(key);

		increaseAvailableCapacity();
		
	}

}
