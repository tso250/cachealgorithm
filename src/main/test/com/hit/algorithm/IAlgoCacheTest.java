package com.hit.algorithm;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import com.hit.algorithm.IAlgoCache;
import com.hit.algorithm.LRUAlgoCacheImpl;
import com.hit.algorithm.NRUAlgoCacheImpl;

class IAlgoCacheTest {

	@Test
	void testLRUAlgoCacheImpl() {
		IAlgoCache<Integer, Integer> cache = new LRUAlgoCacheImpl<Integer, Integer>(3);
		
		Assert.assertEquals(((LRUAlgoCacheImpl<Integer, Integer>)cache).isEmpty(), true);
		Assert.assertEquals(((LRUAlgoCacheImpl<Integer, Integer>)cache).isFull(), false);
		
		cache.removeElement(1);
		cache.removeElement(1);
		cache.removeElement(1);
		
		Assert.assertEquals(((LRUAlgoCacheImpl<Integer, Integer>)cache).isEmpty(), true);
		Assert.assertEquals(((LRUAlgoCacheImpl<Integer, Integer>)cache).isFull(), false);
		
		
		Assert.assertEquals(cache.putElement(1, 1), null);
		Assert.assertEquals(cache.putElement(2, 2), null);
		Assert.assertEquals(cache.putElement(3, 3), null);
		
		Assert.assertEquals(((LRUAlgoCacheImpl<Integer, Integer>)cache).isEmpty(), false);
		
		Assert.assertEquals(cache.putElement(4, 4), new Integer(1));
		Assert.assertEquals(cache.putElement(1, 1), new Integer(2));
		Assert.assertEquals(cache.putElement(2, 2), new Integer(3));
		Assert.assertEquals(cache.putElement(5, 5), new Integer(4));
		
		Assert.assertEquals(((LRUAlgoCacheImpl<Integer, Integer>)cache).isFull(), true);

		Assert.assertEquals(cache.putElement(1, 1), null);
		Assert.assertEquals(cache.putElement(2, 2), null);
		
		Assert.assertEquals(cache.putElement(3, 3), new Integer(5));
		Assert.assertEquals(cache.putElement(4, 4), new Integer(1));
		Assert.assertEquals(cache.putElement(5, 5), new Integer(2));
		
		
		Assert.assertEquals(cache.getElement(6), null);
		Assert.assertEquals(cache.putElement(6, 6), new Integer(3));
		Assert.assertEquals(cache.getElement(6), new Integer(6));
		Assert.assertEquals(cache.putElement(6, 7), null);
		Assert.assertEquals(cache.getElement(6), new Integer(7));
		cache.removeElement(new Integer(6));
		Assert.assertEquals(cache.getElement(6), null);
		
		cache.removeElement(new Integer(6));
		
		Assert.assertEquals(((LRUAlgoCacheImpl<Integer, Integer>)cache).isFull(), false);
		
	}
	
	@Test
	void testNRUAlgoCacheImpl() throws InterruptedException {
		IAlgoCache<Integer, Integer> cache = new NRUAlgoCacheImpl<Integer, Integer>(3);

		Assert.assertEquals(((NRUAlgoCacheImpl<Integer, Integer>)cache).isEmpty(), true);
		Assert.assertEquals(((NRUAlgoCacheImpl<Integer, Integer>)cache).isFull(), false);
		
		cache.removeElement(1);
		cache.removeElement(1);
		cache.removeElement(1);
		
		Assert.assertEquals(((NRUAlgoCacheImpl<Integer, Integer>)cache).isEmpty(), true);
		Assert.assertEquals(((NRUAlgoCacheImpl<Integer, Integer>)cache).isFull(), false);
		
		
		Assert.assertEquals(cache.putElement(1, 1), null);
		Assert.assertEquals(cache.putElement(2, 2), null);
		Assert.assertEquals(cache.putElement(3, 3), null);
		
		Assert.assertEquals(((NRUAlgoCacheImpl<Integer, Integer>)cache).isEmpty(), false);

		cache.getElement(new Integer(1));
		cache.getElement(new Integer(2));
		cache.getElement(new Integer(2));
		cache.getElement(new Integer(1));
		cache.putElement(1, 20);
		cache.putElement(2, 23);
		
		Assert.assertEquals(cache.putElement(4, 4), new Integer(3));
		
	}

}
